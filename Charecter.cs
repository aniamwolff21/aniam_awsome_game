﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public class Character
{
    enum direction {
        left,
        right
    };
    enum state
    {
        standing,
        jumping,
        attacking,
        hurt
    };

    PictureBox character;
    private int jump;
    private int gravity;
    private state characterState;
    private direction characterDirection;
    private int maxHealth, currentHealth;
    private bool collision;

    public Character()
    {
    }

}