﻿namespace MyGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.screen = new System.Windows.Forms.Panel();
            this.stateLabel = new System.Windows.Forms.Label();
            this.block = new System.Windows.Forms.PictureBox();
            this.player = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.PlayerDirection = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.velocity = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.KeyPressed = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CollisionBottom = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CollisionRight = new System.Windows.Forms.Label();
            this.CollisionLeft = new System.Windows.Forms.Label();
            this.screen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.block)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            this.SuspendLayout();
            // 
            // screen
            // 
            this.screen.Controls.Add(this.CollisionLeft);
            this.screen.Controls.Add(this.CollisionRight);
            this.screen.Controls.Add(this.label7);
            this.screen.Controls.Add(this.label6);
            this.screen.Controls.Add(this.CollisionBottom);
            this.screen.Controls.Add(this.label5);
            this.screen.Controls.Add(this.KeyPressed);
            this.screen.Controls.Add(this.label3);
            this.screen.Controls.Add(this.label4);
            this.screen.Controls.Add(this.velocity);
            this.screen.Controls.Add(this.label2);
            this.screen.Controls.Add(this.label1);
            this.screen.Controls.Add(this.PlayerDirection);
            this.screen.Controls.Add(this.stateLabel);
            this.screen.Controls.Add(this.block);
            this.screen.Controls.Add(this.player);
            this.screen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.screen.Location = new System.Drawing.Point(0, 0);
            this.screen.Name = "screen";
            this.screen.Size = new System.Drawing.Size(284, 259);
            this.screen.TabIndex = 0;
            // 
            // stateLabel
            // 
            this.stateLabel.AutoSize = true;
            this.stateLabel.Location = new System.Drawing.Point(96, 26);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(61, 13);
            this.stateLabel.TabIndex = 2;
            this.stateLabel.Text = "PlayerState";
            // 
            // block
            // 
            this.block.BackColor = System.Drawing.Color.Gray;
            this.block.Location = new System.Drawing.Point(89, 211);
            this.block.Name = "block";
            this.block.Size = new System.Drawing.Size(100, 50);
            this.block.TabIndex = 1;
            this.block.TabStop = false;
            this.block.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // player
            // 
            this.player.BackColor = System.Drawing.Color.Transparent;
            this.player.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.player.Image = global::MyGame.Properties.Resources.stand;
            this.player.Location = new System.Drawing.Point(0, 207);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(41, 52);
            this.player.TabIndex = 0;
            this.player.TabStop = false;
            this.player.Click += new System.EventHandler(this.pictureBox1_Click);
            this.player.Paint += new System.Windows.Forms.PaintEventHandler(this.e);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PlayerDirection
            // 
            this.PlayerDirection.AutoSize = true;
            this.PlayerDirection.Location = new System.Drawing.Point(96, 39);
            this.PlayerDirection.Name = "PlayerDirection";
            this.PlayerDirection.Size = new System.Drawing.Size(50, 13);
            this.PlayerDirection.TabIndex = 3;
            this.PlayerDirection.Text = "DIrection";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Player Direction:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Player State:";
            // 
            // velocity
            // 
            this.velocity.AutoSize = true;
            this.velocity.Location = new System.Drawing.Point(96, 52);
            this.velocity.Name = "velocity";
            this.velocity.Size = new System.Drawing.Size(79, 13);
            this.velocity.TabIndex = 6;
            this.velocity.Text = "Player Velocity:";
            this.velocity.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Velocity :";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "KeyPressed :";
            // 
            // KeyPressed
            // 
            this.KeyPressed.AutoSize = true;
            this.KeyPressed.Location = new System.Drawing.Point(96, 65);
            this.KeyPressed.Name = "KeyPressed";
            this.KeyPressed.Size = new System.Drawing.Size(61, 13);
            this.KeyPressed.TabIndex = 9;
            this.KeyPressed.Text = "keypressed";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Collision Bottom:";
            // 
            // CollisionBottom
            // 
            this.CollisionBottom.AutoSize = true;
            this.CollisionBottom.Location = new System.Drawing.Point(96, 78);
            this.CollisionBottom.Name = "CollisionBottom";
            this.CollisionBottom.Size = new System.Drawing.Size(61, 13);
            this.CollisionBottom.TabIndex = 11;
            this.CollisionBottom.Text = "keypressed";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Collision Right:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 91);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Collision Left:";
            // 
            // CollisionRight
            // 
            this.CollisionRight.AutoSize = true;
            this.CollisionRight.Location = new System.Drawing.Point(96, 104);
            this.CollisionRight.Name = "CollisionRight";
            this.CollisionRight.Size = new System.Drawing.Size(70, 13);
            this.CollisionRight.TabIndex = 14;
            this.CollisionRight.Text = "CollisionRight";
            // 
            // CollisionLeft
            // 
            this.CollisionLeft.AutoSize = true;
            this.CollisionLeft.Location = new System.Drawing.Point(96, 91);
            this.CollisionLeft.Name = "CollisionLeft";
            this.CollisionLeft.Size = new System.Drawing.Size(63, 13);
            this.CollisionLeft.TabIndex = 15;
            this.CollisionLeft.Text = "CollisionLeft";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 259);
            this.Controls.Add(this.screen);
            this.Name = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.screen.ResumeLayout(false);
            this.screen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.block)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel screen;
        private System.Windows.Forms.PictureBox player;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox block;
        private System.Windows.Forms.Label stateLabel;
        private System.Windows.Forms.Label PlayerDirection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label velocity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label KeyPressed;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label CollisionBottom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label CollisionLeft;
        private System.Windows.Forms.Label CollisionRight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
    }
}

