﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public enum Direction
{
    left,
    right,
    center
};
public enum State
{
    standing,
    jumping,
    walking,
    attacking,
    hurt
};
public enum keyPressed
{
    none,
    jump,
    right,
    left
}
public enum gameState
{
    StartMenu,
    InGame,
    Paused,
    hurt
};

public class Globals
{
   
    public static PictureBox block;
    public static Panel screen;
    public static keyPressed keyPressed;

}
