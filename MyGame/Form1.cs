﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyGame
{
    public partial class Form1 : Form
    {
        Player player1;

        public Form1()
        {
            InitializeComponent();
            Globals.screen = screen;
            block.Top = screen.Height - block.Height;
            player.Top = screen.Height - player.Height;
            player1 = new Player(player);
            Globals.block = block;
         

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            player1.update(sender, e);
            stateLabel.Text = player1.PlayerState.ToString();
            PlayerDirection.Text = player1.PlayerDirection.ToString();
            velocity.Text = player1.Velocity.ToString();
            KeyPressed.Text = Globals.keyPressed.ToString();
            CollisionBottom.Text = player1.Collision_Bottom().ToString();
            CollisionRight.Text = player1.Collision_Right().ToString();
            CollisionLeft.Text = player1.Collision_Left().ToString();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            player1.updateKeyDown(e);

        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            player1.updateKeyUp(e);
           
        }

        private void e(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
