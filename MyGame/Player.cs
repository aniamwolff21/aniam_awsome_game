﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public class Player : Character
{
    public Player(PictureBox character)
    {
        this.Character1 = character;
    }

    public void updateKeyDown(KeyEventArgs e)
    {
        if (e.KeyCode == Keys.Right)
        {
            Globals.keyPressed = keyPressed.right;
            if (PlayerState != State.jumping)
            {
                PlayerState = State.walking;
            }
            PlayerDirection = Direction.right;
        }
        if (e.KeyCode == Keys.Left)
        {
            Globals.keyPressed = keyPressed.left;
            if (PlayerState != State.jumping)
                PlayerState = State.walking;
            PlayerDirection = Direction.left;
        }

        if (e.KeyCode == Keys.Space && PlayerState != State.jumping)
        {
            Globals.keyPressed = keyPressed.jump;
            PlayerState = State.jumping;
            Velocity = 23;
        }
        if (e.KeyCode == Keys.Space && PlayerState != State.jumping && !InAirNoCollision(Character1))
        {
            
            Globals.keyPressed = keyPressed.jump;//Anti multijump - If the player doesnt jump, is in the air and not colliding with anything
            PlayerState = State.jumping;     //Sets a variable that player is jumping
        }

    }
    public void updateKeyUp(KeyEventArgs e)
    {
        if ((e.KeyCode == Keys.Right || e.KeyCode == Keys.Left))
        {
            PlayerState = State.standing;
            Globals.keyPressed = keyPressed.none;
        }
        if (e.KeyCode == Keys.Space)
        {
            Globals.keyPressed = keyPressed.none;
            
        }


   
    }
}