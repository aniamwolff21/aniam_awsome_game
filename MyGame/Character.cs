﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public class Character
{
    private PictureBox character;
    private int jump = 23;
    private int velocity;
    private State playerState;
    private Direction playerDirection;
    private int maxHealth, currentHealth;
    private bool collision;

    public int Jump
    {
        get { return jump; }
        set { jump = value; }
    }

    public PictureBox Character1
    {
        get { return character; }
        set { character = value; }
    }

    public int Velocity
    {
        get { return velocity; }
        set { velocity = value; }
    }

    public State PlayerState
    {
        get { return playerState; }
        set { playerState = value; }
    }

    public Direction PlayerDirection
    {
        get { return playerDirection; }
        set { playerDirection = value; }
    }

    public int CurrentHealth
    {
        get { return currentHealth; }
        set { currentHealth = value; }
    }

    public int MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }

    public bool Collision
    {
        get { return collision; }
        set { collision = value; }
    }

    public Character()
    {
        velocity = 0;
    }

    public void update(object sender, EventArgs e)
    {

        //walking right
        if (Globals.keyPressed == keyPressed.right && playerDirection == Direction.right && Character1.Right <= Globals.screen.Width && (!Collision_Left()))
        {
            Character1.Left += 5;
            PlayerState = State.walking;
        }

        //walking left
        if (Globals.keyPressed == keyPressed.left && playerDirection == Direction.left && Character1.Left > 0 && (!Collision_Right()))
        {
            Character1.Left -= 5;
            PlayerState = State.walking;
        }

        if (Velocity > 0)
        {   //If any force still exists 
            //Move player up, lower force each "move"
            Velocity--;

            Character1.Top -= 3;
        }
        else
        {   //If no force, player is not jumping.
            if (Velocity == 0 && !Collision_Bottom() && !Collision_Top())
            {
                Character1.Top += 3;
            }
            PlayerState = State.standing;
        }
    }

    public Boolean InAirNoCollision(PictureBox Character1)
    {


        if (!Character1.Bounds.IntersectsWith(Globals.block.Bounds))
        {
            if (Character1.Location.Y < Globals.screen.Width)
            {
                return true;
            }
        }


        return false;
    }



    public Boolean Collision_Top()
    {

        if (Globals.block != null)
        {
            PictureBox temp1 = new PictureBox();
            temp1.Bounds = Globals.block.Bounds;
            temp1.SetBounds(temp1.Location.X - 3, temp1.Location.Y - 1, temp1.Width + 6, 1);
            if (Character1.Bounds.IntersectsWith(temp1.Bounds))
                return true;
        }

        return false;
    }

    public Boolean Collision_Bottom()
    {
        if (Globals.block != null)
        {
            PictureBox temp1 = new PictureBox();
            temp1.Bounds = Globals.block.Bounds;
            temp1.SetBounds(temp1.Location.X, temp1.Location.Y + temp1.Height, temp1.Width, 1);
            if (Character1.Bounds.IntersectsWith(Globals.block.Bounds) || Character1.Bottom >= Globals.screen.Height)
                return true;
        }

        return false;
    }

    public Boolean Collision_Right()
    {

        Rectangle charLeftRect = new Rectangle(Character1.Location.X, Character1.Top + Character1.Height / 4, 2, Character1.Height / 2); 
        if (Globals.block != null)
        {
            PictureBox temp1 = new PictureBox();
            temp1.Bounds = Globals.block.Bounds;
            temp1.SetBounds(temp1.Location.X - 1, temp1.Location.Y + 1, 1, temp1.Height - 1);
            if (charLeftRect.IntersectsWith(Globals.block.Bounds))
                return true;
        }

        return false;
    }
    public Boolean Collision_Left()
    {

        Rectangle charRightRect = new Rectangle(Character1.Location.X + Character1.Width, Character1.Top + Character1.Height / 4, 2, Character1.Height / 2); 
        if (Globals.block != null)
        {
            PictureBox temp2 = new PictureBox();
            temp2.Bounds = Globals.block.Bounds;
            temp2.SetBounds(temp2.Location.X + temp2.Width, temp2.Location.Y + 1, 1, temp2.Height - 1);
            if (charRightRect.IntersectsWith(Globals.block.Bounds))
                return true;
        }

        return false;
    }

}




